var trenutni_vzdevek;
var trenutni_kanal;
var badWordArray;

function getBadWords() {
  var badWordFile = new XMLHttpRequest();
  badWordFile.open('GET', 'swearWords.csv', true);
  badWordFile.onreadystatechange = function() {
    var badWordText = badWordFile.responseText;
    badWordArray = badWordText.split(',');
  }
  badWordFile.send(null);
}

function divElementEnostavniTekst(sporocilo) {
    // ukrademo tage <> in </>
    sporocilo = sporocilo.replace(/</gm, "&lt");
    sporocilo = sporocilo.replace(/>/gm, "&gt");
    // dodamo smajlije
    sporocilo = sporocilo.replace(/:\)/gm,
            "<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png \>");
    sporocilo = sporocilo.replace(/;\)/gm,
            "<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png \>");
    sporocilo = sporocilo.replace(/\(Y\)/gm,
            "<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png \>");
    sporocilo = sporocilo.replace(/:\*/gm,
            "<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png \>");
    sporocilo = sporocilo.replace(/:\(/gm,
            "<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png \>");

    for (var i=0; i<badWordArray.length; i++) {
      regex = new RegExp('(^|[^A-Za-z])' + badWordArray[i] + '($|[^A-Za-z])', 'gim');
      sporocilo = sporocilo.replace(regex, '$1' + Array(badWordArray[i].length+1).join('*') + '$2');
    }

    sporocilo = $('<div style="font-weight: bold"></div>').html(sporocilo);
    return sporocilo;
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  getBadWords();

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    trenutni_vzdevek = rezultat.vzdevek;
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#kanal').text(trenutni_vzdevek + ' @ ' + trenutni_kanal);
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutni_kanal = rezultat.kanal;
    $('#kanal').text(trenutni_vzdevek + ' @ ' + trenutni_kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
